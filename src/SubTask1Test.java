import static org.junit.Assert.*;

import org.junit.Test;

public class SubTask1Test {

	@Test
	public void hasFibonacciSubSquare_MatrixWithFibonacciSquare_True() {
		int[][] matrix = {{2,1,1,2,3},{2,21,0,3,4},{2,13,8,5,6},{2,4,5,6,7},{2,2,77,3,2}};
		assertTrue(SubTask1.hasFibonacciSubSquare(matrix));
	}
	
	@Test
	public void hasFibonacciSubSquare_MatrixWithFibonacciSquare2_True() {
		int matrix[][] = {{0,0,0,0},{0,1,1,2},{0,21,0,3},{0,13,8,5}};
		assertTrue(SubTask1.hasFibonacciSubSquare(matrix));
	}
	
	@Test
	public void hasFibonacciSubSquare_MatrixWithoutFibonacciSquare_False() {
		int matrix[][] = {{1,90,55,14},{7,43,2,1},{1,1,5,8},{1,1,28,13}};
		assertFalse(SubTask1.hasFibonacciSubSquare(matrix));
	}
	
	

}
