import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SubTask1 {

	public static boolean isPerfectSquare(int number) {
		int sqrtNumber = (int) Math.sqrt(number);
		return (sqrtNumber * sqrtNumber == number);
	}

	public static boolean isFibonacciNumber(int number) {
		return isPerfectSquare(5 * number * number + 4) || isPerfectSquare(5 * number * number - 4);
	}

	public static boolean isFibonacciSequence(List<Integer> list) {
		if (!isFibonacciNumber(list.get(0)) || !isFibonacciNumber(list.get(1))) {
			return false;
		}
		for (int i = 2; i < list.size(); i++) {
			if (list.get(i - 2) + list.get(i - 1) != list.get(i)) {
				return false;
			}
		}
		return true;
	}

	public static List<Integer> makeSquare(int[][] matrix, int row, int col, int len) {
		List<Integer> sequence = new ArrayList<>();
		int side = len;
		while (side > 0) {
			sequence.add(matrix[row][col]);
			col++;
			side--;
		}
		col--;
		row++;
		side = len - 1;
		while (side > 0) {
			sequence.add(matrix[row][col]);
			side--;
			row++;
		}
		side = len - 1;
		row--;
		col--;
		while (side > 0) {
			sequence.add(matrix[row][col]);
			col--;
			side--;
		}
		side = len - 2;
		col++;
		row--;
		while (side > 0) {
			sequence.add(matrix[row][col]);
			row--;
			side--;
		}
		return sequence;

	}

	public static boolean hasFibonacciSubSquare(int[][] matrix) {
		for (int i = 0; i < matrix.length - 1; i++) {
			for (int j = 0; j < matrix[0].length - 1; j++) {
				int rowLen = matrix.length - i;
				int colLen = matrix.length - j;
				int maxLen;
				if (rowLen < colLen) {
					maxLen = rowLen;
				} else {
					maxLen = colLen;
				}
				for (int k = 2; k <= maxLen; k++) {
					List<Integer> list = makeSquare(matrix, i, j, k);
					if (isFibonacciSequence(list)) {
						return true;
					}
				}
			}
		}
		return false;
	}
}
