import java.util.Collections;
import java.util.List;

public class SubTask3 {

	public static int sumOfBits(int number) {
		int sum = 0;
		if (number < 0) {
			sum = 1;
			number = Math.abs(number);
		}
		while (number > 0) {
			sum += number % 2;
			number = number / 2;
		}
		return sum;
	}

	public static int compareBySumOfBits(Integer a, Integer b) {
		int sumA = sumOfBits(a);
		int sumB = sumOfBits(b);
		if (sumA > sumB) {
			return 1;
		}
		if (sumA < sumB) {
			return -1;
		}
		return 0;
	}

	public static List<Integer> sortArrayBySumOfBits(List<Integer> array) {
		array.sort(SubTask3::compareBySumOfBits);
		return array;
	}

}
