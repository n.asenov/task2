import static org.junit.Assert.*;
import org.junit.Test;

public class SubTask2Test {

	@Test
	public void hasNumberWithFibonacciSequenceNeighbours_MatrixWithFibonacciSequenceNeighbours_True() {
		int[][] matrix = { { 1, 5, 8, 13, 8, 12 }, { 2, 144, 1, 21, 9, 13 }, { 3, 89, 55, 34, 10, 1 },
				{ 4, 5, 6, 7, 11, 15 } };
		assertTrue(SubTask2.hasNumberWithFibonacciSequenceNeighbours(matrix));
		
	}

	@Test
	public void hasNumberWithFibonacciSequenceNeighbours_MatrixWithoutFibonacciSequenceNeighbours_False() {
		int[][] matrix = { { 1, 5, 9, 13, 8 }, { 2, 144, 1, 21, 13 }, { 3, 56, 34, 10, 1 }, { 4, 6, 7, 12, 15 } };
		assertFalse(SubTask2.hasNumberWithFibonacciSequenceNeighbours(matrix));
	}


}
