import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class SubTask3Test {

	@Test
	public void sortArrayBySumOfBits_ArrayWithPositiveNumbers_SortedArray() {
		List<Integer> list = new ArrayList<>();
		list.add(5);
		list.add(11);
		list.add(45);
		list.add(67);
		list.add(9);
		list = SubTask3.sortArrayBySumOfBits(list);
		List<Integer> expected = new ArrayList<>();
		expected.add(5);
		expected.add(9);
		expected.add(11);
		expected.add(67);
		expected.add(45);
		assertArrayEquals(expected.toArray(), list.toArray());
	}

	@Test
	public void sortArrayBySumOfBits_ArrayWithNegativeNumbers_SortedArray() {
		List<Integer> list = new ArrayList<>();
		list.add(-6);
		list.add(12);
		list.add(-15);
		list.add(67);
		list.add(-45);
		list = SubTask3.sortArrayBySumOfBits(list);
		List<Integer> expected = new ArrayList<>();
		expected.add(12);
		expected.add(-6);
		expected.add(67);
		expected.add(-15);
		expected.add(-45);
		assertArrayEquals(expected.toArray(), list.toArray());
	}

}
