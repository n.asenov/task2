import java.util.*;

public class SubTask2 {
	public static boolean isFibonacciSequence(List<Integer> list) {
		Collections.sort(list);
		for (int i = 2; i < list.size(); i++) {
			if (list.get(i - 2) + list.get(i - 1) != list.get(i)) {
				return false;
			}
		}
		return true;
	}

	public static List<Integer> getNeighboursOfNumber(int[][] matrix, int row, int col) {
		List<Integer> list = new ArrayList<>();
		int rowLength = matrix.length;
		int colLength = matrix[0].length;

		if (row - 1 >= 0) {
			if (col - 1 >= 0) {
				list.add(matrix[row - 1][col - 1]);
			}
			if (col + 1 < colLength) {
				list.add(matrix[row - 1][col + 1]);
			}
			list.add(matrix[row - 1][col]);
		}
		if (row + 1 < rowLength) {
			if (col - 1 >= 0) {
				list.add(matrix[row + 1][col - 1]);
			}
			if (col + 1 < colLength) {
				list.add(matrix[row + 1][col + 1]);
			}
			list.add(matrix[row + 1][col]);
		}
		if (col - 1 >= 0) {
			list.add(matrix[row][col - 1]);
		}

		if (col + 1 < colLength) {
			list.add(matrix[row][col + 1]);
		}
		return list;
	}

	public static boolean hasNumberWithFibonacciSequenceNeighbours(int[][] matrix) {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				List<Integer> list = getNeighboursOfNumber(matrix, i, j);

				if (isFibonacciSequence(list)) {
					return true;
				}
			}
		}
		return false;
	}

}
